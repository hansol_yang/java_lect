import java.util.Scanner;


public class Reservation {
    // field
    static Scanner sc = new Scanner(System.in);
    static int row, col;

    public static void main(String[] args) {
        boolean[][] isReserved = new boolean[10][10]; // false면 예약 가능, true면 예약 불가능
        String option = new String();

        while(true) {
            showStatus(isReserved);
            System.out.print("예약을 하시겠습니까? (y/n)");
            option = new Scanner(System.in).next();
            switch(option) {
                case "y" :
                    reserve(isReserved);
                    break;

                case "n" :
                    System.out.println("예약 시스템을 종료합니다.");
                    System.exit(1);
                    break;

                case "reset" :
                    reset(isReserved);
                    break;

                default:
                    System.out.println("잘못된 입력입니다.");
                    break;
            }
        }
    }

    public static void reserve(boolean[][] bArr) {
        System.out.print("좌석의 행: ");
        row = (int)sc.next().charAt(0) - 97;
        System.out.print("좌석의 열: ");
        col = sc.nextInt() - 1;

        if(bArr[row][col] == true) {
            System.out.println("이미 예약된 좌석입니다. 좌석을 다시 선택하세요.");
        }
        else {
            System.out.println((char)(row + 97) + "행 " + (col + 1) + "열 예약 성공 !");
            bArr[row][col] = true;
        }
    }

    public static void showStatus(boolean[][] bArr) {
        // ascii a : 97 - j : 106
        System.out.print("   ");
        for(int i = 1; i <= bArr.length; i ++) {
            System.out.print(i + "  ");
        }
        System.out.println();
        for(int i = 0; i < bArr.length; i ++) {
            System.out.print((char)(i + 97) + "  ");
            for(int j = 0; j < bArr[i].length; j ++) {
                if(bArr[i][j] == true) {
                    System.out.print("X  ");
                }
                else {
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
    }

    public static void reset(boolean[][] bArr) {
        for(int i = 0; i < bArr.length; i ++) {
            for(int j = 0; j < bArr[i].length; j ++) {
                bArr[i][j] = false;
            }
        }
        System.out.println("좌석 현황이 초기화 되었습니다.");
    }
}
