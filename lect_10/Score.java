public class Score {
    public static void main(String[] args) {
        // 2차원 배열
        int[][] scores = new int[10][3];

        // 초기화 해주지 않아도 기본적으로 0이 들어간다.
        // 학생 4의 수학 성적
        // System.out.println(scores[3][2]);

        // 학생 7의 영어 성적
        // System.out.println(scores[6][1]);

        // 모든 학생의 성적
        for(int i = 0; i < scores.length; i ++) { // 2차원 배열의 length는 행의 길이를 의미한다.
            System.out.println((i+1) + "번 학생 성적: ");
            for(int j = 0; j < scores[i].length; j ++) { // 열의 길이는 i번째 행의 길이로 참조한다.
                System.out.println(scores[i][j]);
            }
        }
    }
}
