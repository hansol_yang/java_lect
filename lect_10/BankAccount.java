public class BankAccount {
    // field
    private int balance;

    // default constructor
    public BankAccount() {
        this(0);
    }

    // custom constructor
    public BankAccount(int b) {
        if(b >= 0) this.balance = b;
    }

    public int getBalance() {
        return this.balance;
    }

    public static void main(String[] args) {
        BankAccount[] ba = new BankAccount[3];
        for(int i = 0; i < ba.length; i++) {
            ba[i] = new BankAccount(1000);
        }

        for(int i = 0; i < ba.length; i++) {
            System.out.println(ba[i].getBalance());
        }
    }
}
