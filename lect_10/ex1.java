// 학생의 성적을 저장
// 사용자로부터 학생수 입력받기
// 학생수만큼의 성적을 저장할 수 있는 배열 생성
import java.util.Scanner;


class ex1 {
    public static void main(String[] args) {
        int number;
        double[] scores;

        Scanner sc = new Scanner(System.in);

        System.out.print("학생 수: ");
        number = sc.nextInt();
        scores = new double[number];

        System.out.println("생성된 배열의 길이는 " + scores.length + " 입니다.");
        for(int i = 0; i < scores.length; i++) {
            System.out.print("성적 입력( "+(i+1)+"번 학생 ): ");
            scores[i] = sc.nextDouble();
        }


        System.out.printf("학생 성적의 평균은 %.2f", mean(scores));

        // System.out.println("입력하신 학생들의 성적은");
        // for(int i = 0; i < scores.length; i++) {
        //     System.out.println((i+1) + "학생: " + scores[i]);
        // }
    }

    // 학생들의 성정이 저장된 배열을 매개변수로 받아 평균을 반환하는 메소드
    public static double mean(double[] dArr) {
        double sum = 0;

        // For 사용
        // for(int i = 0; i < dArr.length; i++) {
        //     sum += dArr[i];
        // }

        // ForEach 사용
        for(double x : dArr) {
            sum += x;
        }

        return sum/dArr.length;
    }
}

class ex1_2 {
    public static void main(String[] args) {
        int[] iArr_1 = {1,2,3,4,5,6,7,8,9,10};
        int[] iArr_2 = new int[iArr_1.length];

        for(int i = 0; i < iArr_1.length; i ++) {
            iArr_2[i] = iArr_1[i];
        }

        System.out.println("배열의 길이는 " + iArr_2.length);
    }
}
