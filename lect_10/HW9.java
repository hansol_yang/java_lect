import java.util.Random;


class HW9_1 {
    public static void main(String[] args) {
        int[] grade = new int[10];
        int tmp;
        Random r = new Random();

        for(int i = 0; i < 50; i ++) {
            do {
                tmp = r.nextInt(101);
            }while(tmp % 5 != 0 || (tmp/5)%2 == 0);

            if(tmp == 5) {
                grade[0] ++;
            }
            else {
                String index = Integer.toString(tmp).substring(0,1);
                grade[Integer.parseInt(index)] ++;
            }
        }

        for(int i = 50; i >= 1; i --) {
            for(int j = 0; j < grade.length; j ++) {
                if(grade[j] >= i) {
                    System.out.print("*\t");
                }
                else System.out.print(" \t");
            }
            System.out.println();
        }
        System.out.print("5\t15\t25\t35\t45\t55\t65\t75\t85\t95\n");
    }
}

class HW9_2 {
    public static void main(String[] args) {
        String tmp = new String();

        if(args.length > 0) {
            for(int i = 0; i < args.length; i ++) {
                for(int j = 1; i + j < args.length; j ++) {
                    if(Integer.compare(args[i].length(), args[i+j].length()) == 0) {
                        // 현재 값의 길이와 다음 값의 길이가 같다.
                        if(args[i].compareTo(args[i+j]) > 0) {
                            tmp = args[i];
                            args[i] = args[i+j];
                            args[i+j] = tmp;
                        }
                    }
                    else if(Integer.compare(args[i].length(), args[i+j].length()) < 0) {
                        // 현재 값의 길이가 다음 값의 길이보다 짧다.
                        tmp = args[i];
                        args[i] = args[i+j];
                        args[i+j] = tmp;
                    }
                    else {
                        // 현재 값의 길이가 다음 값의 길이보다 길다.
                    }
                }
                System.out.print(args[i] + " ");
            }
            System.out.println();
        }
        else {
            System.exit(1);
        }
    }
}
