public class HW6_2{
    public static void setCoord(int x, int y) {
        System.out.println("x, y coordinates: (" + x + ", " + y + ")");
    }

    public static void setCoord(int x, int y, int z) {
        System.out.println("x, y coordinates: (" + x + ", " + y + ", " + z + ")");
    }

    public static void main(String[] args) {
        setCoord(6,3);
        setCoord(3,6,2);
    }
}
