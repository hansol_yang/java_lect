public class Car {
    private int mileage = 0;

    public void goStraight(int d) {
        System.out.printf("%dm 직진했습니다.\n", d);
        mileage += d;
    }

    public void turnLeft() {
        System.out.print("좌회전 후 ");
    }

    public void turnRight() {
        System.out.print("우회전 후 ");
    }

    public int getMileage() {
        return this.mileage;
    }
}

class TestCar {
    public static void main(String[] args) {
        Car c = new Car();
        c.goStraight(25);
        c.turnLeft();
        c.goStraight(30);
        c.turnRight();
        c.goStraight(15);
        c.turnLeft();
        c.goStraight(20);
        System.out.printf("총 주행거리는 %dm입니다.\n", c.getMileage());
    }
}
