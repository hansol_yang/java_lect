public class Student {
    protected String name;
    protected int age;
    protected double score;

    public Student() {}

    public Student(String name, int age, double score) {
        this.name = name;
        this.age = age;
        this.score = score;
    }

    @Override
    public String toString() {
        return String.format("이름: %s\n나이: %d\n학점: %.2f", this.name, this.age, this.score);
    }
}

class TestStudent {
    public static void main(String[] args) {
        Student hong = new Student("홍길동", 25, 4.3);
        System.out.println(hong);
    }
}
