public class StringManipulate {
    public StringManipulate() {};

    public String concat(String a, String b) {
        return a + b;
    }

    public void toUpper(String a) {
        for(int i = 0; i < a.length(); i++) {
            if(a.charAt(i) >= 'a' && a.charAt(i) <= 'z') {
                System.out.print((char)(a.charAt(i) - 32));
            }
            else {
                System.out.print(a.charAt(i));
            }
        }
        System.out.println();
    }

    public boolean equalTest(String a, String b) {
        if(a.length() != b.length()) return false;
        else {
            for(int i = 0; i < a.length(); i++) {
                if(a.charAt(i) != b.charAt(i)) return false;
            }
        }
        return true;
    }
}
