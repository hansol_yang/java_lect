import java.util.Scanner;


public class Search {
    private String target;
    private String keyword;
    private Scanner sc = new Scanner(System.in);

    public String setTarget() {
        System.out.print("문장: ");
        this.target = sc.nextLine();
        return this.target;
    }

    public String setKeyword() {
        System.out.print("검색할 단어: ");
        this.keyword = sc.nextLine();
        return this.keyword;
    }

    public void searchKeyword() {
        // System.out.println(this.setTarget());
        // System.out.println(this.setKeyword());
        int cnt = 0;
        int sentence = 1;
        this.setTarget();
        this.setKeyword();

        System.out.print("검색 결과: ");
        for(int i = 0; i + this.keyword.length() < this.target.length(); i ++) {
            if(target.substring(i, i + this.keyword.length()).charAt(0) == ' ') {
                sentence ++;
            }
            if(this.keyword.equalsIgnoreCase(target.substring(i, i + this.keyword.length()))) {
                cnt ++;
                if(i + this.keyword.length() == this.target.length() - 1) {
                    System.out.print(sentence + "번째 어절에서 ");
                }
                else {
                    System.out.print(sentence + "번째, ");
                }
            }
        }
        System.out.printf("\"%s\"가 검색되어, 총 %d회 검색되었습니다.\n", this.keyword, cnt);
    }
}

class TestSearch {
    public static void main(String[] args) {
        Search s1 = new Search();

        s1.searchKeyword();
    }
}
