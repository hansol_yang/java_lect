import java.util.Random;

class TestExamGrade {
    public static void main(String[] args) {
        int[] score = new int[50];
        int[] grade = new int[10];
        int height;
        Random r = new Random();

        for(int i = 0; i < score.length; i ++) {
            score[i] = r.nextInt(101);
            if(score[i] == 100) {
                grade[9] += 1;
            }
            else {
                grade[score[i]/10] += 1;
            }
        }

        height = grade[0];
        for(int i = 0; i + 1 < grade.length; i ++) {
            height = Math.max(height, grade[i+1]);
        }

        for(int i = height ; i >= 1; i--) {
            for(int j = 0; j < grade.length; j ++) {
                if(grade[j] >= i) {
                    System.out.print(" * ");
                }
                else {
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
        System.out.println(" 5 15 25 35 45 55 65 75 85 95 ");
    }
}
