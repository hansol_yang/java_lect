public class Player {
    private String name;
    private int score = 0;

    public Player() {}

    public Player(String name) {
        this.name = name;
    }

    public String getPlayerName() {
        return this.name;
    }

    public int getPlayerScore() {
        return this.score;
    }

    public int play() {
        int tmp;
        tmp = (int)(Math.random()*6 + 1);
        this.score += tmp;
        return tmp;
    }
}

class TestPlayer {
    public static void main(String[] ars) {
        Player p1 = new Player("홍길동");
        Player p2 = new Player("장발장");

        for(int i = 0; i < 5; i ++) {
            System.out.printf("%d회차 결과: %s (%d), %s (%d)\n", i+1, p1.getPlayerName(), p1.play(), p2.getPlayerName(), p2.play());
        }

        if(p1.getPlayerScore() > p2.getPlayerScore()) {
            System.out.printf("최종 결과\n승자: 홍길동(%d)\n패자: 장발장(%d)\n", p1.getPlayerScore(), p2.getPlayerScore());
        }
        else if(p1.getPlayerScore() < p2.getPlayerScore()){
            System.out.printf("최종 결과\n승자: 장발장(%d)\n패자: 홍길동(%d)\n", p2.getPlayerScore(), p1.getPlayerScore());
        }
        else {
            System.out.println("최종 결과\n무승부!!");
        }
    }
}
