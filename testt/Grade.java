public class Grade {
    private double sum;

    public Grade() {};

    public double averageCal(double... score) {
        for(double x : score) {
            sum += x;
        }

        return sum/score.length;
    }
}
