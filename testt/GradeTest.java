public class GradeTest {
    public static void main(String[] args) {
        Grade g1 = new Grade();
        Grade g2 = new Grade();
        double avg;

        avg = g1.averageCal(90.3, 80.6, 98.5);
        System.out.printf("1번 학생의 평균 점수는 %.2f\n", avg);

        avg = g2.averageCal(70.3, 50.80);
        System.out.printf("2번 학생의 평균 점수는 %.2f\n", avg);
    }
}
