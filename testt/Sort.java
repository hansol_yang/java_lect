public class Sort {
    public static void main(String[] args) {
        if(args != null) {
            for(int i = 0; i < args.length; i++) {
                for(int j = 1; i + j < args.length; j ++) {
                    if(args[i].length() == args[i+j].length()) {
                        if(args[i].compareTo(args[i+j]) > 0) {
                            String tmp = args[i+j];
                            args[i+j] = args[i];
                            args[i] = tmp;
                        }
                    }
                    else if(args[i].length() < args[i+j].length()) {
                        String tmp = args[i+j];
                        args[i+j] = args[i];
                        args[i] = tmp;
                    }
                }
            }

            for(int i = 0; i < args.length; i++) {
                System.out.println(args[i] + " ");
            }
        }
    }
}
