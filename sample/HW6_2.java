import java.util.Scanner;


public class HW6_2{
	public static void main(String[] args){
		String keyword = "preStoredString"; //미리 저장된 문자열
		Scanner s = new Scanner(System.in);
		System.out.print("문자열을 입력하세요:");
		String message = s.nextLine();
		
		if(message.equals(keyword)) System.out.print("문자열 일치"); //문자열 내용이 일치하는지 판단하는 메소드 equals를 사용
		//message.equals(keyword)는 message가 참조하는 문자열과 keyword가 참조하는 문자열 내용이 일치하는지 여부를 boolean형으로  반환		
		else System.out.print("문자열 불일치"); 
		
		s.close();
	}
}
