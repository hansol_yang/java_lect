import java.util.Scanner;


class hw2_1 {
	public static void main(String[] args) {
		int userInput, remainder;
		String[] units = {"만","천","백","십", "\0"};
		int[] vals = new int[5];
		Scanner sc = new Scanner(System.in);
		System.out.print("일억 미만의 정수 : ");
		userInput = sc.nextInt();

		vals[0] = userInput/10000;
		remainder = userInput%10000;

		vals[1] = remainder/1000;
		remainder = remainder%1000;

		vals[2] = remainder/100;
		remainder = remainder%100;

		vals[3] = remainder/10;
		vals[4] = remainder%10;
		for (int i = 0; i < units.length; i++) {
			if(vals[i] != 0) {
				System.out.printf("%d%s  ", vals[i], units[i]);	
			}
		}
		System.out.println("입니다.");
	}
}

class hw2_2 {
	public static void main(String[] args) {
		int userInput, abs, sign, quetient, remainder;
		Scanner sc = new Scanner(System.in);

		System.out.print("integer : ");
		userInput = sc.nextInt();
		
		sign = (userInput>>31)&1;
		abs = userInput;
		if(sign == 1) {
			abs = ~userInput + 1;
		}
		quetient = (abs>>3)|0;
		remainder = abs&7;

		System.out.printf("\"%d의 절댓값은 %d이고,\n", userInput, abs);
		System.out.printf("\t그 수를 8로 나눈 몫은 %d이고,\n", quetient);
		System.out.printf("\t\t나머지는 %d입니다.\"\n", remainder);
	}
}

class hw2_3 {
	public static void main(String[] args) {
		double x1, y1, x2, y2, width, height, round, area;
		Scanner sc = new Scanner(System.in);

		System.out.print("x1 : ");
		x1 = sc.nextDouble();
		System.out.print("y1 : ");
		y1 = sc.nextDouble();
		System.out.printf("점1 : (%f , %f)\n", x1, y1);

		System.out.print("x2 : ");
		x2 = sc.nextDouble();
		System.out.print("y2 : ");
		y2 = sc.nextDouble();
		System.out.printf("점2 : (%f , %f)\n", x2, y2);

		width = x2 - x1;
		height = y2 - y1;
		if(width < 0) {
			width *= -1;
		}
		if(height < 0) {
			height *= -1;
		}
		round = 2*width + 2*height;
		area = width*height;
		System.out.printf("round : %.1f, area : %.1f \n", round, area);

	}
}