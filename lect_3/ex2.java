import java.util.Scanner;
import java.util.Random;


class ex2 {
	public static void main(String[] args) {
		// Random class => require import
		// Math class => in java.lang package
		Random r = new Random();
		int dap = 10 + r.nextInt(6); // 10 ~ 15
		Scanner sc = new Scanner(System.in);
		int userInput;

		while(true) {
			System.out.print("10 - 15 : ");
			userInput = sc.nextInt();

			if(dap < userInput) {
				System.out.println("Down!");
			}
			else if(dap > userInput) System.out.println("Up!");
			else {
				System.out.println("CORRECT!!");
				break;
			}
		}
	}
}

class ex2_2 {
	public static void main(String[] args) {
		int r = (int)((Math.random())*10);
		System.out.println(r);
	}
}

class ex2_3 {
	public static void main(String[] args) {
	}
}