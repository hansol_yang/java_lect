import java.util.Scanner;


public class ex1 {
	public static void main(String[] args) {
		// 만약 온도가 30이상이라면 "덥다." 출력
		// 20도 이상이라면 "따뜻하다."
		// 아니라면 춥다
		double degree;
		Scanner sc = new Scanner(System.in);

		while(true) {
			System.out.println("온도: ");
			degree = sc.nextDouble();
			if(degree >= 30) {
				System.out.println("덥다.");
			}
			else if(degree >= 20) {
				System.out.println("따뜻하다.");	
			}
			else {
				System.out.println("춥다.");	
			}
		}
	}
}