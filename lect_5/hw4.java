import java.util.Scanner;


class hw4_1 {
	public static void main(String[] args) {
		int x, result;

		for(x = 1; x < 20; x += 2) {
			if(x <= 3) {
				result = (x * x * x) - (9 * x) + 2;
				System.out.println("x: " + x + ", f(x): " + result);
			}
			else {
				result = (2 * x) - 4;
				System.out.println("x: " + x + ", f(x): " + result);
			}
		}
	}
}

class hw4_2 {
	public static void main(String[] args) {
		String type;
		int rows, cols, iter;
		Scanner sc = new Scanner(System.in);

		
		while(true) {
			System.out.print("< Type >\npyramid\nrectangular\ndiamond\n");
			System.out.print("type: ");
			type = sc.next();

			switch (type) {
				case "pyramid":
					System.out.print("stages: ");
					rows = sc.nextInt();
					for(int i = 0; i < rows; i++) {
						for(int j = 0; j < rows - i; j++) {
							System.out.print(" ");
						}
						for(int k = i; k >= 0; k--) {
							System.out.print(k);
						}
						for(int l = 1; l <= i; l++) {
							System.out.print(l);
						}
						System.out.println();
					}
					break;

				case "rectangular":
					System.out.print("rows: ");
					rows = sc.nextInt();
					System.out.print("cols: ");
					cols = sc.nextInt();
					System.out.print("iter: ");
					iter = sc.nextInt();

					for(int k = 0; k < rows; k++) {
						for(int j = 0; j < iter; j++) {
							for(int i = 0; i < cols; i++) {
								System.out.print("*");
							}
							System.out.print(" ");	
						}	
						System.out.println();
					}
					break;

				case "diamond":
					System.out.print("rows(odd-only): ");
					rows = sc.nextInt();

					if(rows % 2 == 0) {
						System.out.println("Odd-Only");
						break;
					}
					else {
						for(int i = 1; i <= (int)(rows/2); i++) {
							for(int k = 0; k <= (int)(rows/2) - i; k++) {
								System.out.print(" ");
							}
							for(int j = 0; j < 2*i - 1; j++) {
								System.out.print("*");
							}
							System.out.println();
						}
						for(int l = 1; l <= rows; l++) {
							System.out.print("*");
						}
						System.out.println();
						for(int m = (int)(rows/2); m >= 1; m--) {
							for(int n = (int)(rows/2) - m; n >= 0; n--) {
								System.out.print(" ");
							}
							for(int o = 2*m - 1; o > 0; o--) {
								System.out.print("*");
							}
							System.out.println();
						}
						
						break;
					}
					
				default:
					System.out.println("pyramid, rectangular, and diamond types are available");
					break;
			}	
		}
	}
}

class hw4_3 {
	public static void main(String[] args) {
		int cols;
		Scanner sc = new Scanner(System.in);

		System.out.print("cols: ");
		cols = sc.nextInt();

		for(int i = 2; i <= 9; i+=cols) {
			for(int j = 1; j <= 9; j++) {
				for(int k = 0; k < cols; k++) {
					if((i+k) > 9) break;
					System.out.printf("%d * %d = %d\t", (i+k), j, (i+k)*j);	
				}
				System.out.println();
			}
			System.out.println();
		}
	}
}