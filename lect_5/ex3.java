import java.util.Scanner;


class ex3_1 {
	public static void main(String[] args) {
		int i = 1;
		int a;
		Scanner sc = new Scanner(System.in);

		System.out.print("a: ");
		a = sc.nextInt();

		while(i < 10) {
			System.out.println(a + " * " + i + " = " + (a*i));
			i++;
		}
	}
}

class ex3_2 {
	public static void main(String[] args) {
		int a, b, larger, smaller, gcd, remainder;
		Scanner sc = new Scanner(System.in);


		System.out.print("a : ");
		a = sc.nextInt();
		System.out.print("b : ");
		b = sc.nextInt();

		if(b > a) {
			larger = b;
			smaller = a;
		}
		else if(a > b) {
			larger = a;
			smaller = b;
		}
		else {
			larger = a;
			smaller = a;
		}

		while(true) {
			if(smaller == 0) {
				System.out.println("gcd(" + a + ", "+ b + ") = " + larger);
				break;
			}
			remainder = larger % smaller;
			larger = smaller;
			smaller = remainder;
		}

		
	}
}

class ex3_3 {
	public static void main(String[] args) {
		int a, i;
		long result = 1L;
		Scanner sc = new Scanner(System.in);

		System.out.print("a: ");
		a = sc.nextInt();
		i = a;
		if(a == 0 || a > 20) {
			System.out.println("Error");
			return;
		}

		while(true) {
			result *= i;
			i--;
			if(i == 0) {
				System.out.println(a + "'s factorial is " + result);
				break;
			}
		}
	} 
}

class ex3_4 {
	public static void main(String[] args) {
		int rows = 5;
		for(int i = 1; i <= rows; i ++) {
			for(int j = 0; j < rows - i; j ++) {
				System.out.print(" ");
			}
			for(int k = 0; k < 2*i -1; k++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}