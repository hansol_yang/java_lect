class ex1_1 {
	public static void main(String[] args) {
		int diff = 4;
		int sum = 3;

		for(int i = 0; i < 5; i++) {
			System.out.println(sum);
			sum += diff;
		}
	}
} 

class ex1_2 {
	public static void main(String[] args) {
		for(int i = 3; i < 100; i += 4) {
			System.out.println(i);
		}
	}
}