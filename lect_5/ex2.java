import java.util.Scanner;


class ex2_1 {
	public static void main(String[] args) {
		for(int i = 0; i < 5; i++) {
			for(int j = 0; j < 10; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}

class ex2_2 {
	public static void main(String[] args) {
		int rows, cols;
		Scanner sc = new Scanner(System.in);

		System.out.print("rows: ");
		rows = sc.nextInt();
		System.out.print("cols: ");
		cols = sc.nextInt();

		for(int i = 0; i < rows; i++) {
			for(int j = 0; j < cols; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}

// float left
class ex2_3 {
	public static void main(String[] args) {
		int rows;
		Scanner sc = new Scanner(System.in);

		System.out.print("rows: ");
		rows = sc.nextInt();

		for(int i = 1; i < rows + 1; i++) {
			for(int j = 1; j <= i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}

// float right
class ex2_4 {
	public static void main(String[] args) {
		int rows;
		Scanner sc = new Scanner(System.in);

		System.out.print("rows: ");
		rows = sc.nextInt();

		for(int i = 1; i < rows + 1; i++) {
			for(int k = 0; k < rows - i; k++) {
				System.out.print(" ");
			}
			for(int j = 1; j <= i; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}

// pyramid
class ex2_5 {
	public static void main(String[] args) {
		int rows;
		Scanner sc = new Scanner(System.in);

		System.out.print("rows: ");
		rows = sc.nextInt();

		for(int i = 1; i <= rows; i ++) {
			for(int j = 1; j <= rows - i; j ++) {
				System.out.print(" ");
			}
			for(int k = 1; k <= 2 * i - 1; k ++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}

// rev pyramid
class ex2_6 {
	public static void main(String[] args) {
		int rows;
		int num = 0;
		Scanner sc = new Scanner(System.in);

		System.out.print("rows: ");
		rows = sc.nextInt();

		for(int i = rows; i > 0; i--) {
			num++;
			for(int j = 0; j <= rows - i; j++) {
				System.out.print(" ");
			}
			for(int k = 0; k < rows - num; k++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}


class ex2_7 {
	public static void main(String[] args) {
		int a, result;
		Scanner sc = new Scanner(System.in);

		System.out.print("a: ");
		a = sc.nextInt();

		for(int i = 1; i < 10; i++) {
			result = i*a;
			System.out.println(a + " * " + i + " = " + result);
		}

	}
}

class ex2_8 {
	public static void main(String[] args) {
		for(int i = 2; i <= 9; i++) {
			for(int j = 1; j < 10; j++) {
				System.out.println(i + " * " + j + " = " + (i*j));
			}
			System.out.println("-----------");
		}
	}
}

class ex2_9 {
	public static void main(String[] args) {
		for(int i = 1; i < 11; i ++) {
			System.out.print(i + "'s divisors are ");
			for(int j = 1; j < 11; j ++) {
				if(i % j == 0) {
					System.out.print(j + " ");
				}
			}
			System.out.println();
		}
	}
}