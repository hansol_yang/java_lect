public class Member {
    // field
    private String name = new String();
    private int age;
    private static int numberOfMembers = 0;

    // default constructor
    public Member() {
        numberOfMembers ++;
    }
    // custom constructor
    public Member(String name, int age) {
        this.name = name;
        this.age = age;
        numberOfMembers ++;
    }

    // getter
    public static int getNumberOfMembers() {
        return numberOfMembers;
    }

    public static void main(String[] args) {
        Member m1, m2, m3;
        m1 = new Member();
        m2 = new Member();
        m3 = new Member();
        int number;

        number = Member.getNumberOfMembers();
        System.out.printf("현재까지 생성된 멤버의 수는 %2d명 입니다.\n", number);
    }
}
