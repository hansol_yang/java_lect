public class Figure {
    protected double round;
    protected double area;

    // default constructor
    public Figure() {}

    // custom constructor
    public Figure(double round, double area) {
        this.round =  round;
        this.area = area;
    }

    // toString method
    @Override
    public String toString() {
        return String.format("이 도형은 %s이고, 둘레는 %.2f, 넓이는 %.2f입니다.\n", this.getClass().getName(), this.round, this.area);
    }
}

class Circle extends Figure {
    protected double radius;

    public Circle() {}

    public Circle(double radius) {
        super(2*Math.PI*radius, Math.PI*radius*radius);
        this.radius = radius;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("이 원의 반지름은 %.1f", this.radius);
    }
}

class EquilateralTriangle extends Figure {
    protected double segment;
    protected double height;

    public EquilateralTriangle() {}

    public EquilateralTriangle(double segment, double height) {
        super(3*segment, 0.5*segment*height);
        this.segment = segment;
        this.height = height;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("이 삼각형의 한 변의 길이는 %.2f이고, 높이는 %.2f입니다.", this.segment, this.height);
    }
}

class TestFigure {
    public static void main(String[] args) {
        Circle c = new Circle(2);
        System.out.println(c);
        EquilateralTriangle e = new EquilateralTriangle(2, 1.73);
        System.out.println(e);
    }
}
