public class Person {
    // field
    protected int age;
    protected String name = new String();

    // default constructor
    public Person() {}

    // custom constructor
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // method
    public void eat(String menu, int number) {
        System.out.printf("%s을(를) %d개 먹습니다.\n", menu, number);
    }

    @Override
    public String toString() {
        return "< " + this.getClass().getName() + " >\n" + "이름: " + this.name + "\n나이: " + this.age;
    }
}

class Player extends Person{
    // default constructor
    public Player() {}

    // custom constructor
    public Player(String name, int age) {
        super(name, age);
    }
    // method
    public void play() {
        System.out.println("연주합니다.");
    }
}

class Director extends Person{
    // default constructor
    public Director() {}
    // custom constructor
    public Director(String name, int age) {
        super(name, age);
    }
    // method
    public void direct() {

    }
}

class TestPerson {
    public static void main(String[] args) {
        Player p1 = new Player("장발장", 25);
        Director d1 = new Director("홍길동", 30);

        p1.eat("피자", 3);
        System.out.println(p1);
        System.out.println(d1);
    }
}
