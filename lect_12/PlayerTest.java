class Player {
    // field : 이름, 나이
    String name = new String();
    int age;

    // default cosntructor
    Player() {

    }

    // custom constructor
    Player(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // method: 연주하는 메소드(play)
    void play() {
        System.out.println("연주합니다.");
    }
}

class Pianist extends Player{
    String typeOfPiano; // 피아노의 제조회사
    // 피아니스트 생성자
    Pianist() {

    }

    Pianist(String name, int age, String brand) {
        // super.name = name;
        // super.age = age;
        super(name, age);
        this.typeOfPiano = brand;
    }
    @Override
    void play() {
        System.out.print(this.name + "이 " + this.typeOfPiano + "의 피아노를");
        super.play();
    }
}

class Cellist extends Player{
    @Override
    void play() {
        System.out.print("첼로를 ");
        super.play();
    }
}

class Violinist extends Player {
    @Override
    void play() {
        System.out.print("바이올린을 ");
        super.play();
    }
}
