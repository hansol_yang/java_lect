public class StringM {
    // field
    private String tmp = new String();

    // method
    // 1. 하나의 문자열을 매개변수로 받아 그 역순 문자열을 반환하는 메소드
    // 2. 두 문자열을 매개변수로 받아 내용이 정확하게 일치하는지 여부를 반환하는 메소드
    // 3. 문자열 하나를 매개변수로 받고 그 문자열의 대문자만 소문자로 바꿔 출력하는 메소드
    public String rev(String s) {
        for(int i = s.length() - 1; i >= 0; i--) {
            tmp += s.charAt(i);
        }

        return tmp;
    }

    public boolean equalTest(String s1, String s2) {
        if(s1.length() != s2.length()) {
            return false;
        }
        else {
            for(int i = 0; i < s1.length(); i ++) {
                if(s1.charAt(i) != s2.charAt(i)) {
                    return false;
                }
            }
        }
        return true;
    }

    public void caseTest(String s) {
        for(int i = 0; i < s.length(); i++) {
            if(s.charAt(i) >= 'A' && s.charAt(i) <= 'Z') {
                System.out.print((char)(s.charAt(i) + 32));
            }
            else {
                System.out.print(s.charAt(i));
            }
        }
        System.out.println();
    }
}

class Test7 {
    public static void main(String[] args) {
        StringM sm = new StringM();
        String rev = new String();
        boolean status;

        rev = sm.rev("Hello World");
        status = sm.equalTest("Hello world", "Hello World");
        sm.caseTest("HelLo WoRld");
        // System.out.println(status ? "일치" : "불일치");
    }
}
