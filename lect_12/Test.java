class Test {
    public static void main(String[] args) {
        // 연주자 3명 생성
        // 지휘자에게 지휘할 것을 요청

        Conductor c1 = new Conductor("양한솔", 25);
        c1.direct();

    }
}

 class Conductor {
    String name;
    int age;
    Player[] list; // 지휘자가 관리하는 연주자들

    Conductor() {}
    Conductor(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void direct() {
        // 리스트에 있는 연주자들에게 모두 play() 메소드 호출
        Pianist p = new Pianist("홍길동", 25, "영창피아노");
        p.play();
        Violinist v = new Violinist();
        v.play();
        Cellist c = new Cellist();
        c.play();
    }
}
