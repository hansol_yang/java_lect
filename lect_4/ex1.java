// 임금 계산 문제
import java.util.Scanner;


// 8시간까지는 시간당 5,000원
// 8시간 초과분에 대해서는 1.5배 지급 
public class ex1 {
	public static void main(String[] args) {
		final int payment = 5000;
		int time;
		int income;
		Scanner sc = new Scanner(System.in);
		System.out.print("일한 시간: ");
		time = sc.nextInt();
		if(time <= 8) {
			income = payment*time;
		}
		else {
			income = (8*payment) + (int)((time-8)*(payment*1.5));
		}
		System.out.print("임금: " + income);
	}
}