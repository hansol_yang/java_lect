// 문자열 하나 or 둘 or셋을 입력 받아 a 에 1 or 2 or 3 대임
import java.util.Scanner;


class ex4 {
	public static void main(String[] args) {
		String input = "";
		Scanner sc = new Scanner(System.in);
		int a;

		while(true) {
			System.out.print("입력: ");
			input = sc.next();
			switch (input) {
				case "하나":
					a = 1;
					System.out.printf("a = %d\n", a);
					break;
				case "둘":
					a = 2;
					System.out.printf("a = %d\n", a);
					break;
				case "셋":
					a = 3;
					System.out.printf("a = %d\n", a);
					break;
				default:
					break;
			}	
		}
	}
}