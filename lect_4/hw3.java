import java.util.Scanner;


class hw3_1 {
	public static void main(String[] args) {
		double a, b;
		String operator;
		Scanner sc = new Scanner(System.in);

		System.out.println("두 수와 연산자 (+, -, *, %, /)를 입력으로 받아들여 이를 계산하여 출력하는 프로그램");
		System.out.print("a: ");
		a = sc.nextDouble();
		System.out.print("b: ");
		b = sc.nextDouble();
		System.out.print("연산자: ");
		operator = sc.next();

		// System.out.printf("%f %s %f\n", a, operator, b);
		switch (operator) {
			case "+":
				System.out.printf("%.2f\n", a+b);
				break;

			case "-":
				System.out.printf("%.2f\n", a-b);
				break;

			case "*":
				System.out.printf("%.2f\n", a*b);
				break;

			case "/":
				System.out.printf("%.2f\n", a/b);
				break;

			case "%": 
				System.out.printf("%d\n", (int)(a%b));
				break;

			default: 
				break;
		}
	}
}

class hw3_2 {
	public static void main(String[] args) {
		int sum = 0;
		int i = 0;
		int tmp;
		Scanner sc = new Scanner(System.in);

		

		while(true) {
			System.out.print("정수: ");
			tmp = sc.nextInt();

			if(tmp % 2 != 0) {
				// odd
				if(tmp % 7 == 0 || tmp == 1111) {
					System.out.println("7의 배수나 1111은 평균 계산에서 제외 됩니다.");
				}
				else {
					i++;
					sum += tmp;
					System.out.println("평균 : " + sum/i);
				}
			}
			else {
				//even
				System.out.println("Even Number !!");
			}
		}
	}
}

class hw3_3 {
	public static void main(String[] args) {
		int a;
		int result = 0;
		String tmp;
		Scanner sc = new Scanner(System.in);

		System.out.print("1보다 큰 수 하나를 입력: ");
		a = sc.nextInt();

		for(int i = 1; i <= a; i++) {
			if(i % 2 == 0) {
				tmp = "-" + i;
				result -= i*i;
			}
			else if(i == 1) {
				tmp = "1";
				result += 1;
			}
			else {
				tmp = "+" + i;
				result += i*i;
			}
			System.out.print(tmp + "^2");
		}
		System.out.println(" = " + result);
	} 
}

class hw3_4 {
	public static void main(String[] args) {
		double score;
		Scanner sc = new Scanner(System.in);

		while(true) {
			System.out.print("점수: ");
			score = sc.nextDouble();

			if(score >= 90 && score <= 100) {
				System.out.println("수");
			}
			else if(score >= 80 && score < 90) {
				System.out.println("우");
			}
			else if(score >= 70 && score < 80) {
				System.out.println("미");
			}
			else if(score >= 60 && score < 70) {
				System.out.println("양");
			}
			else if(score >= 0 && score < 60) {
				System.out.println("가");
			}
			else {
				System.out.println("정확한 점수를 입력하시오.");
			}	
		}
	}
}