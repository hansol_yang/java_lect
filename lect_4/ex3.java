// 자동차 면허 문제
import java.util.Scanner;


public class ex3 {
	public static void main(String[] args) {
		int score;
		int type = -1;
		Scanner sc = new Scanner(System.in);

		while(true) {
			System.out.print("< 면허 시험 종류 선택 >\n1. 1종 면허\n2. 2종 면허\n입력: ");
			type = sc.nextInt();
			switch(type) {
				case 1:
					System.out.print("점수 입력: ");
					score = sc.nextInt();
					if(score >= 70) {
						System.out.println("합격 입니다.");
						break;
					}
					else {
						System.out.println("불합격 입니다.");
						break;
					}
				case 2: 
					System.out.print("점수 입력: ");
					score = sc.nextInt();
					if(score >= 60) {
						System.out.println("합격 입니다.");
						break;
					}
					else {
						System.out.println("불합격 입니다.");
						break;
					}
				default: 
					System.out.println("잘못된 입력입니다.");
					break;
			}
		}
	}
}