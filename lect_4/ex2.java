// 중간 점검 문제 1,3
import java.util.Scanner;


class ex2_1 {
	public static void main(String[] args) {
		double n;
		Scanner sc = new Scanner(System.in);

		System.out.print("입력: ");
		n = sc.nextDouble();

		if(n >= 100) {
			System.out.print("large");
		}
		else {
			System.out.print("small");	
		}
	}
}

class ex2_2 {
	public static void main(String[] args) {
		double size;
		Scanner sc = new Scanner(System.in);

		System.out.print("cup size: ");
		size = sc.nextDouble();

		if(size < 100) {
			System.out.print("small");	
		}	
		else if(size >= 100 && size < 200) {
			System.out.print("medium");
		}
		else {
			System.out.print("large");
		}
	}
}