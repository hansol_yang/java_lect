class Piano {
    // properties
    private String name;
    private String[] musics; // 연주할 수 있는 곡들..
    private String grade; // 실력


    // default constructor
    public Piano() {
        this("이름 없음");
    }

    // custom constructor
    public Piano(String name) {
        this.name = name;
    }

    public Piano(String[] list) {
        this.musics = list;
    }

    public Piano(String name, String[] list, String grade) {
        this(name);
        this.musics = list;
        this.grade = grade;
    }

    // setter
    public void setMusics(String[] list) {
        this.musics = list;
    }

    // getter
    public void getMusics() {
        // for나 for-each 사용하라

        // 배열의 길이가 아예 없을 수도 있으므로 길이 체크가 아니라 널 체크를 해야한다.
        System.out.print("연주 가능한 곡은 ");
        if(this.musics != null) {
            for(int i = 0; i < this.musics.length; i++) {
                System.out.print(this.musics[i] + " ");
            }
            System.out.println("입니다.");
        }
        else {
            System.out.print("없습니다. 연주 가능한 곡을 등록해주세요.");
        }
    }


    // method

    // 곡 이름을 매개 변수로 받아서 연주하는 play() 메소드
    public void play(String music) {
        boolean status = false;

        if(this.grade != null && this.grade.equalsIgnoreCase("상")) {
            System.out.println(music + "을(를) 연주합니다.");
        }
        else {
            if(this.musics != null) {
                for(int i = 0; i < this.musics.length; i ++) {
                    if(this.musics[i].equalsIgnoreCase(music)) {
                        status = true;
                    }
                }
            }

            if(status) {
                System.out.println(music + "을(를) 연주합니다.");
            }
            else {
                System.out.println(music + "을(를) 연주할 수 없습니다.");
            }
        }
    }
}
