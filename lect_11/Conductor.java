public class Conductor {
    public static void main(String[] args) {
            String[] musics = {"호랑나비", "으르렁", "크리스마스"};

            Piano p1 = new Piano("베토벤");

            // 곡 3개를 연주할 수 있는 연주자 홍길동 생성
            Piano p2 = new Piano("홍길동", musics, "상");

            // p2.getMusics();
            // p1.getMusics();
            p1.play("에라 모르겠다.");
            p1.play("호랑나비");
            p2.play("호랑나비");
            p2.play("에라 모르겠다.");
    }
}
