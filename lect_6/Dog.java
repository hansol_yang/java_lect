class Dog {
	// properties
	private int size = 0;
	private int full = 0; // 0(hungry) ~ 10(full)
	private String sp;

	// COMPARE SIZE METHOD
 	public boolean sizeEquals(Dog d) {
 		return size == d.getSize();
	}

	public void setSize(int s) { // setter
		// CHECK VALIDATION
		if(s < 0 || s > 100) size = 5;
		else size = s;
	}

	public int getFull() { //getter
		return full;
	}

	public int getSize() {
		return size;
	}

	// actions: function(=method)
	void eat(int f) {
		full += f;
		if(full > 8) 
			System.out.println("im full!!");
		else if(full > 3)
			System.out.println("enough");
		else if(full > 0)
			System.out.println("im stil hungry");
		else
			System.out.println("r u kidding me ?");
	}

	void play() {
		System.out.println("funny");
	}

	void speak() {
		System.out.println("hi");
	}
}