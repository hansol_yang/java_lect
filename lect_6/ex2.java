import java.util.Scanner;


class ex2_1 {
	public static void main(String[] args) {
		final int answer = 10;
		int userInput;
		Scanner sc = new Scanner(System.in);

		do {
			System.out.print("guess: ");
			userInput = sc.nextInt();

			if(userInput > answer)
				System.out.println("UP");
			else if(userInput < answer)
				System.out.println("DOWN");
			else {
				System.out.println("CORRECT!!");
				break;
			}
		}while(true);		
	}
}