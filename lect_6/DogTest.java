class DogTest {
	public static void main(String[] args) {
		Dog myDog = new Dog();
		Dog yourDog = new Dog();
		
		myDog.setSize(-9);
		System.out.println(myDog.getSize());
		System.out.println(yourDog.getSize());
		System.out.println(myDog.sizeEquals(yourDog));

		if(myDog.getFull() >= 5) myDog.play();
		else myDog.eat(5);
	}
}