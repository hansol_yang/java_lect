import java.util.Scanner;


class ex1_1 {
	public static void main(String[] args) {
		int rows, space;
		Scanner sc = new Scanner(System.in);

		System.out.print("rows: ");
		rows = sc.nextInt();
		System.out.print("space: ");
		space = sc.nextInt();

		for(int i = 1; i <= rows; i++) {
			for(int j = 1; j <= rows - i; j++) {
				System.out.print(" ");
			}
			for(int k = 1; k <= i; k++) {
				System.out.print("*");
			}
			for(int j = 1; j <= space; j++) {
				System.out.print(" ");
			}
			for(int k = 1; k <= i; k++) {
				System.out.print("*");
			}
			System.out.println();
		}
		for(int i = rows; i >= 1; i--) {
			for(int j = 1; j <= rows - i; j++) {
				System.out.print(" ");
			}
			for(int k = 1; k <= i; k++) {
				System.out.print("*");
			}
			for(int j = 1; j <= space; j++) {
				System.out.print(" ");
			}
			for(int k = 1; k <= i; k++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}

class ex1_2 {
	public static void main(String[] args) {
		int n = 12;
		while (n > 0) {
		n -= 2;
		if( n == 6 )
		break;
		System.out.println(n);
		}
	}
}