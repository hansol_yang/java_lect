import java.util.Scanner;


class hw5_1 { // 1.A
	public static void main(String[] args) {
		int num = 0;

		for(int i = 0; i <= 100; i++) {
			if(num == 5) {
				num = 0;
				System.out.println();
				continue;
			}
			if(i % 2 == 0 || i % 3 == 0 || i % 5 == 0 || i % 7 == 0) {
				continue;
			}
			System.out.printf("%2d ", i);
			num++;
		}
		System.out.println();
	}
}

class hw5_2 { // 1.B prime number
	public static void main(String[] args) {

	}
}

class hw5_3 {
	public static void main(String[] args) {
		String userInput;
		char c;
		int count1 = 0, count2 = 0;
		Scanner sc = new Scanner(System.in);

		System.out.print("Input: ");
		userInput = sc.nextLine();

		for(int i = 0; i < userInput.length(); i++) {
			c = userInput.charAt(i);
			if((c>='a'&&c<='z')||(c>='A'&&c<='Z')) {
				if(c=='a'||c=='e'||c=='i'||c=='o'||c=='u'||c=='A'||c=='E'||c=='I'||c=='O'||c=='U') {
					count1++;
				}
				else {
					count2++;
				}
			}
		}
		System.out.println(count1);
		System.out.println(count2);
	}
}

class hw5_4 {
	public static void main(String[] args) {
		int rows, cols, iter;
		Scanner sc = new Scanner(System.in);

		System.out.print("rows: ");
		rows = sc.nextInt();
		System.out.print("cols: ");
		cols = sc.nextInt();
		System.out.print("iter: ");
		iter = sc.nextInt();

		if((rows > 1 && rows < 20) && (cols > 1 && cols < 20) || (iter > 1 && iter < 20)) {
			for(int i = 0; i < rows; i ++) {
				for(int k = 0; k < iter; k ++) {
					for(int j = 0; j < cols; j ++) {
						System.out.print("*");
					}
					System.out.print("\t");	
				}
				System.out.println();
			}
		}	
		else {
			System.out.println("1 < [rows, cols] < 20");
		}
	}
}
