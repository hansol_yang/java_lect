import java.util.Scanner;

class hw1_1 {
	public static void main(String[] args) {
		String name;
		String phone;
		Scanner sc = new Scanner(System.in);
		System.out.print("이름 : ");
		name = sc.nextLine();
		System.out.print("전화번호 : ");
		phone = sc.nextLine();

		System.out.printf("이름 : %s, 전화번호: %s", name, phone);

		// System.out.printf("%s", name);
	}
}

class hw1_2 {
	public static void main(String[] args) {
		int a;
		int b;
		Scanner sc = new Scanner(System.in);
		System.out.println("양의 정수 2개를 입력받습니다.");
		System.out.print("첫번째 : ");
		a = sc.nextInt();
		System.out.print("두번째 : ");
		b = sc.nextInt();
		System.out.println("수\t\t10진수\t\t8진수\t\t16진수");
		System.out.printf("첫째수%10d\t\t%8o\t\t%8x\n", a, a, a);
		System.out.printf("둘째수%10d\t\t%8o\t\t%8x\n", b, b, b);
		System.out.printf("합계%10d\t\t%8o\t\t%8x\n", a+b, a+b, a+b);
	}
}

class hw1_3 {
	public static void main(String[] args) {
		String name;
		int age;
		double weight;
		Scanner sc = new Scanner(System.in);

		System.out.print("이름: ");
		name = sc.nextLine();
		System.out.print("나이: ");
		age = sc.nextInt();
		System.out.print("몸무게: ");
		weight = sc.nextDouble();

		System.out.printf("\"나의 이름은 '%s',\n나이는 %#-15x,\n\t몸무게는 %-10.3f 이다.\"", name, age, weight);
	}
}