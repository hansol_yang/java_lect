import java.util.Scanner;

public class ex1 {
	public static void main(String[] args) {
		int inCome;
		int result = 0;
		Scanner sc = new Scanner(System.in);
		System.out.print("월급 : ");
		inCome = sc.nextInt();
		result = inCome * 12 * 10;
		System.out.printf("10년간 모인 금액은 %d원", result);
	}
}

class ex2 {
	public static void main(String[] args) {
		double radius;
		double result;
		Scanner sc = new Scanner(System.in);
		System.out.print("원의 반지름 : ");
		radius = sc.nextDouble();
		result = radius*radius*3.141592;
		System.out.printf("원의 면적: %f", result);
	}
}

class ex3 {
	public static void main(String[] args) {
		double width;
		double height;
		double area;
		double round;
		int op;

		Scanner sc = new Scanner(System.in);
		while(true) {
			System.out.println("직사각형의 넓이와 둘레를 계산하는 프로그램입니다");
			System.out.print("가로: ");
			width = sc.nextDouble();
			System.out.print("세로: ");
			height = sc.nextDouble();

			System.out.print("원하는 출력은?\n1. 넓이\n2. 둘레");
			op = sc.nextInt();

			if(op == 1) {
				area = width*height;
				System.out.printf("넓이 : %f", area);
			}
			else if(op == 2) {
				round = 2*width + 2*height;
				System.out.printf("둘레 : %f", round);
			}
			else {
				return;
			}	
		}
	}
}