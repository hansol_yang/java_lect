import java.util.Scanner;


class ex2_1 {
    public static void main(String[] args) {
        String input = new String();
        String rev = new String();
        Scanner sc = new Scanner(System.in);


        System.out.print("입력: ");
        input = sc.nextLine();

        for(int i = input.length() - 1; i >= 0; i --) {
            rev += input.charAt(i);
        }

        System.out.println(rev);
    }
}

class ex2_2 {
    public static void main(String[] args) {
        String input = new String();
        String target = new String("HELLO WORLD");
        Scanner sc = new Scanner(System.in);

        System.out.print("입력: ");
        input = sc.nextLine();

        if(target.equals(input)) {
            System.out.println("일치");
        }
        else {
            System.out.println("불일치");
        }

    }
}
