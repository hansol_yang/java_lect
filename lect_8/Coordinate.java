public class Coordinate {
    // default constructor
    public Coordinate() {};

    // 2차원
    public void setCoord(int x, int y) {
        System.out.println("x, y coordinates: (" + x + ", " + y + ")");
    };

    // 3차원
    public void setCoord(int x, int y, int z) {
        System.out.println("x, y, z coordinates: (" + x + ", " + y + ", " + z + ")");
    }
}
