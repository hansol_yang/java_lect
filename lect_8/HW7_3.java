import java.util.Scanner;


public class HW7_3 {
    public static void main(String[] args) {
        StringManipulate sm = new StringManipulate();
        String s1, s2;
        int option;
        Scanner sc = new Scanner(System.in);

        while(true) {
            System.out.print("\n< 원하는 기능 선택 >\n1. 두 문자열 연결\n2. 대문자 출력\n3. 같은 문자열인지 체크\n입력(숫자로): ");
            option = sc.nextInt();

            switch(option) {
                case 1:
                    System.out.print("< 두 문자열 연결 기능 >\n첫번째 문자열: ");
                    s1 = sc.next();
                    System.out.print("두번째 문자열: ");
                    s2 = sc.next();
                    if(s1 != "\0" && s2 != "\0") {
                        System.out.println("결과: " + sm.appendString(s1, s2));
                        break;
                    }
                    else break;

                case 2:
                    System.out.print("< 대문자 출력 기능 >\n입력: ");
                    s1 = sc.next();
                    if(s1 != "\0") {
                        System.out.println("결과: " + sm.toCapital(s1));
                        break;
                    }
                    else break;

                case 3:
                    System.out.print("< 같은 문자열인지 체크하는 기능 >\n첫번째 문자열: ");
                    s1 = sc.next();
                    System.out.print("두번째 문자열: ");
                    s2 = sc.next();
                    if(s1 != "\0" && s2 != "\0") {
                        System.out.println(sm.equivalent(s1, s2));
                        break;
                    }
                    else break;

                default:
                    System.out.println("정확한 기능 번호를 입력해주세요.");
                    break;
            }
        }
    }
}
