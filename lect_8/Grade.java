public class Grade {
    // field
    private double sum;
    private double mean;

    // default constructor
    public Grade() {}

    // method
    public double averageCal(double... score) {
        for(double x : score) {
            sum += x;
        }
        return mean = sum / score.length;
    }
}
