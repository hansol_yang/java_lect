// String class의 length(), charAt() 메소드를 제외한 메소드는 사용하지 말 것.
public class StringManipulate {
    // field
    private char tmp;
    private boolean equality;
    private String result = new String();

    // default constructor
    public StringManipulate() {};

    public String appendString(String a, String b) {
        return a + b;
    }

    public String toCapital(String a) {
        // return a.toUpperCase();
        result = "";

        for(int i = 0; i < a.length(); i ++) {
            for(char c = 'A'; c <= 'Z'; c ++) {
                if(a.charAt(i) == c) tmp = c;
            }
            for(char c = 'a'; c <= 'z'; c ++) {
                if(a.charAt(i) == c) tmp = (char)((int)c - 32);
            }
            result += tmp;
        }
        return result;
    }

    public boolean equivalent(String a, String b) {
        equality = true;
        // CHECK LENGTH
        if(a.length() != b.length()) {
            equality = false;
        }
        else {
            for(int i = 0; i < a.length(); i ++) {
                if(a.charAt(i) != b.charAt(i)) {
                    equality = false;
                }
            }
        }

        return equality;
    }
}
