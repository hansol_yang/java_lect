import java.util.Scanner;


class Date {
	private int year;
	private String month;
	private int day;

	public Date() {
		month = "1��";
		day = 1;
		year = 2009;
	}

	public Date(int year, String month, int day) {
		setDate(year, month, day);
	}

	public Date(int year) {
		setDate(year, "1��", 1);
	}

	public void setDate(int year, String month, int day) {
		this.year = year;
		this.month = month;
		this.day = day;
	}

	public String toString() {
		return year + "�� " + month + " " + day + "��";
	}
}