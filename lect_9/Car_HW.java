public class Car_HW {
    // field
    private int mileage;

    // default constructor
    public Car_HW() {
            this.mileage = 0;
    }

    // method
    public void goStraight(int d) {
        System.out.println(d + "m 직진했습니다.");
        this.mileage += d;
    }

    public void turnLeft() {
        System.out.print("좌회전 후 ");
    }

    public void turnRight() {
        System.out.print("우회전 후 ");
    }

    public int getMileage() {
        return this.mileage;
    }

}
