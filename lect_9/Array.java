// 학생 10명의 성적(실수)을 입력 받아서 평균 출력하기

import java.util.Scanner;


public class Array{
    public static void main(String[] args) {
        double[] scores = new double[10];
        double sum = 0;
        Scanner sc = new Scanner(System.in);

        for(int i = 0; i < scores.length; i ++) {
            System.out.print("성적: ");
            scores[i] = sc.nextDouble();
            sum += scores[i];
        }

        System.out.println("학생들의 평균: " + sum/scores.length);

    }
}
