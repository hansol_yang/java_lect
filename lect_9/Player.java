public class Player {
    private String name = new String();
    private int[] score = new int[5];


    // default constructor
    public Player() {
        this("홍길동");
    };

    // custom constructor
    public Player(String name) {
        this.name = name;
    };

    // getter
    public int[] getInfo() {
        return this.score;
    }

    public String getName() {
        return this.name;
    }

    public void play() {
        for(int i = 0; i < 5; i ++) {
                this.score[i] = (int)(Math.random()*6) + 1;
        }
    }


}
