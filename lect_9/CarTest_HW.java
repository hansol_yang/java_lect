public class CarTest_HW {
    public static void main(String[] args) {
        Car_HW c1 = new Car_HW();

        // 예제 1.
        c1.goStraight(25);
        c1.turnLeft();
        c1.goStraight(30);
        c1.turnRight();
        c1.goStraight(15);
        c1.turnLeft();
        c1.goStraight(20);
        System.out.println("총 주행거리는 " + c1.getMileage() + "m 입니다.");

        // 예제 .2
        c1.goStraight(10);
        c1.turnRight();
        c1.goStraight(9);
        System.out.println("총 주행거리는 " + c1.getMileage() + "m 입니다.");
    }
}
