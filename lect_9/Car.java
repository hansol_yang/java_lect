public class Car {
    private String color;
    private int age;

    // 생성자 2개
    // 1. 디폴트 생성자: 매개변수가 하나도 없는 생성자
    //    2번 생성자를 호출할 것.
    // 2. 매개변수 2개를 받는 생성자


    // default constuctor
    public Car() {
        this("red", 0); // this == constuctor
    }

    // custom constuctor
    public Car(String color, int age) {
        this.color = color;
        this.age = age;
    }

}
