import java.util.Scanner;


public class Search {
    private String target = new String("가는 말이 고와야 오는 말이 곱다. 발 없는 말이 천리 간다.");
    private String keyword = new String("발이");
    private String tmp = new String();
    private int cnt; // 총 몇 회 검색되었는지?
    private int index; // 몇 번째 어절에서 검색되는지?
    Scanner sc = new Scanner(System.in);

    private String targetInput() {
        System.out.print("문장: ");
        return sc.nextLine();
    }

    private String keywordInput() {
        System.out.print("검색할 단어: ");
        return sc.nextLine();
    }

    public void search() {
        this.cnt = 0;
        this.index = 1;

        this.target = this.targetInput();
        this.keyword = this.keywordInput();

        System.out.print("검색 결과: ");

        for(int i = 0; i + keyword.length() <= target.length(); i ++) {
            this.tmp = target.substring(i, i+keyword.length());
            // if(tmp.indexOf(" ") == 0) cnt ++;
            if(tmp.indexOf(" ") == 0) this.index ++;

            if(tmp.equalsIgnoreCase(this.keyword)) {
                this.cnt++;
                System.out.print(this.index + "번째 ");
            }
        }
        
        if(cnt != 0) {
                System.out.println("어절에서 " + "\"" + this.keyword + "\"가 검색되어, 총 " + cnt + "회 검색되었습니다." );
        }
        else {
            System.out.println("총 0회 검색되었습니다." );
        }

    }
}
