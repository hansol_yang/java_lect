public class Student {
    private String name = new String();
    private int age;
    private double score;
    // default constructor
    public Student() {
        this("홍길동", 0, 0);
    };

    // custom constructor
    public Student(String name, int age, double score) {
        this.name = name;
        this.age = age;
        this.score = score;
    };

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public double getScore() {
        return this.score;
    }

    public String getInfo() {
        return "\n< 학생 정보 >\n이름: " + this.name + "\n나이: " + this.age + "\n학점: " + this.score; 
    }
}
