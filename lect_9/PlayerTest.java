public class PlayerTest {
    public static void main(String[] args) {
        // 객체
        Player hong = new Player();
        Player jang = new Player("장발장");
        // field
        int sum1 = 0, sum2 = 0;

        // System.out.println((int)(Math.random()*6) + 1);

        hong.play();
        jang.play();

        for(int i = 0; i < 5; i ++) {
            System.out.printf("%d회차 결과: %s (%d), %s (%d)\n", i+1, hong.getName(), hong.getInfo()[i], jang.getName(), jang.getInfo()[i]);
            sum1 += hong.getInfo()[i];
            sum2 += jang.getInfo()[i];
        }
        System.out.println("\n최종 결과");
        if(sum1 > sum2) {
            System.out.println("승자: 홍길동 (" + sum1 + ")");
            System.out.println("패자: 장발장 (" + sum2 + ")");
        }
        else if(sum1 < sum2) {
            System.out.println("승자: 장발장 (" + sum2 + ")");
            System.out.println("패자: 홍길동 (" + sum1 + ")");
        }
        else {
            System.out.println("무승부 !!\n점수: " + sum1);
        }


    }
}
