import java.util.Scanner;


public class StudentTest {
    public static void main(String[] args) {
        String name = new String();
        int age;
        double score;
        Student s1;
        Scanner sc = new Scanner(System.in);

        System.out.print("이름: ");
        name = sc.nextLine();

        System.out.print("나이: ");
        age = sc.nextInt();

        System.out.print("학점: ");
        score = sc.nextDouble();

        s1 = new Student(name, age, score);
        System.out.println("학생1이 생성되었습니다.");

        System.out.println(s1.getInfo());
    }
}
