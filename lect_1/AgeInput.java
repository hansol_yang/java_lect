// 사용자로부터 나이를 입력받아 그대로 출력하는 프로그램 작성
// 클래스이름은 AgeInput
import java.util.Scanner;

public class AgeInput { // export default 
	public static void main(String[] args) {
		int age; // 나이를 저장할 공간 확보
		int tel;
		double height; 

		tel = 9508340;
		Scanner sc = new Scanner(System.in);
		System.out.print("나이: ");
		age = sc.nextInt();
		System.out.print("나이는 " + age + "입니다");
	}
}