import java.util.Scanner;


class test_2 {
	public static void main(String[] args) {
		int a, b;
		Scanner sc = new Scanner(System.in);

		System.out.print("a: ");
		a = sc.nextInt();
		System.out.print("b: ");
		b = sc.nextInt();

		System.out.printf("a+b: %+15d\n", a+b);
		System.out.printf("a*b: %#15o\n", a*b);
		System.out.printf("a-b: %#x\n", a-b);
	}
}