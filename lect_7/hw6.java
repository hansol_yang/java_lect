import java.util.Scanner;


class hw6_1 {
	public static void main(String[] args) {
		String userInput = new String();
		String storage = new String();
		Scanner sc = new Scanner(System.in);

		System.out.print("Input: ");
		userInput = sc.nextLine();

		for(int i = userInput.length() - 1; i >= 0 ; i --) {
			storage += userInput.charAt(i);
		}
		System.out.println(storage);
	}
}

class hw6_2 {
	public static void main(String[] args) {
		String init = "Hello World";
		String userInput = new String();
		Scanner sc = new Scanner(System.in);

		System.out.print("Input: ");
		userInput = sc.nextLine();

		if(init.equals(userInput)) {
			System.out.println("Equals");
		}
		else if(!(init.equals(userInput))) {
			System.out.println("Different");
		}
		else {
			System.out.println("Quit ..");
		}
	}
}