import java.util.Scanner;


class test_3 {
	public static void main(String[] args) {
		double a;
		int i, abs, quotient, remainder;
		Scanner sc = new Scanner(System.in);

		System.out.print("a: ");
		a = sc.nextDouble();

		i = (int)a;


		if(i>>31 == -1) {
			abs = ~i + 1;
		}
		else {
			abs = i;
		}

		quotient = abs >> 5;
		remainder = abs & 31;

		System.out.println(i + "'s abs: " + abs);
		System.out.println("quotient: " + quotient);
		System.out.println("remainder: " + remainder);

	}
}