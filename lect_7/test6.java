import java.util.Scanner;


class test_6 {
	public static void main(String[] args) {
		int userInput;
		int fact = 1;
		Scanner sc = new Scanner(System.in);


		while(true) {
			System.out.print("user input: ");
			userInput = sc.nextInt();
			if(userInput == -1) break;
			else if(userInput == 0 || userInput % 6 == 0) continue;
			else {
				for(int i = userInput; i >= 1; i--) {
					fact *= i;
				}
				System.out.println(userInput + "!: " + fact);
				fact = 1;
			}
		}
	}
}