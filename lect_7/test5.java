import java.util.Scanner;


class test_5 {
	public static void main(String[] args) {
		int rows, space;
		Scanner sc = new Scanner(System.in);

		System.out.print("rows: ");
		rows = sc.nextInt();
		System.out.print("space: ");
		space = sc.nextInt();

		if((space >= 1 && space <= 5) && (rows >= 3 && rows <= 9)) {
			for(int i = 1; i <= rows; i ++) {
				for(int j = 1; j <= rows - i; j ++) {
					System.out.print(" ");
				}
				for(int k = 1; k <= i; k ++) {
					System.out.print("*");
				}
				for(int j = 1; j <= space; j ++) {
					System.out.print(" ");
				}
				for(int k = 1; k <= i; k ++) {
					System.out.print("*");
				}
				System.out.println();
			}
			for(int i = 1; i <= space; i ++) {
				System.out.println();
			}
			for(int i = 0; i < rows; i ++) {
				for(int j = 0; j < i; j ++) {
					System.out.print(" ");
				}
				for(int k = 0; k < rows - i; k ++) {
					System.out.print("*");
				}
				for(int j = 1; j <= space; j ++) {
					System.out.print(" ");
				}
				for(int k = 0; k < rows - i; k ++) {
					System.out.print("*");
				}
				System.out.println();
			}	
		}
		else {
			System.out.println("Invalid args");
		}
	}
}