import java.util.Scanner;


class left {
	public static void main(String[] args) {
		int rows, half; 
		Scanner sc = new Scanner(System.in);

		System.out.print("rows: ");
		rows = sc.nextInt();
		
		half = rows / 2;

		for(int i = 1; i <= half; i ++) {
			for(int j = 1; j <= i - 1; j ++) {
				System.out.print(" ");
			}
			for(int k = 1; k <= 1; k ++) {
				System.out.print("*");
			}
			for(int k = 1; k <= rows - 2; k += 2) {
				System.out.print(" ");
			}
			for(int k = 1; k <= 1; k ++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}