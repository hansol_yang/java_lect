class Animal {

}

class Dog extends Animal {

}

class SmallDog extends Dog {

}

class Person {

}

public class AnimalTest {
    public static void main(String[] args) {
        Animal a = new Animal();
        Dog d = new Dog();
        SmallDog sd = new SmallDog();

        System.out.println(a instanceof Animal); // true
        System.out.println(a instanceof Dog); // false // 뒤에 오는 클래스가 참조 변수의 자식 클래스라면 거짓.
        System.out.println(d instanceof Animal); // true
        System.out.println(sd instanceof Animal); // true // 뒤에 오는 클래스가 부모 클래스이거나 참조 변수의 클래스이면 참.
        System.out.println(sd instanceof Dog); // true
        System.out.println(sd instanceof SmallDog); // true
        System.out.println(d instanceof SmallDog); // false
        // System.out.println(d instanceof Person); // false // 상속 관계가 없으므로 컴파일 오류 발생.
    }
}
