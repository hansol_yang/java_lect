import java.util.*;


class Arr {
    private static int age = 0;

    public static int inputAge() throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.print("나이: ");
        age = sc.nextInt();
        return age;
    }

    public static void main(String[] args) throws Exception {
        try {
            int result = inputAge();
            System.out.println(result);
        }catch (Exception e) {
            System.err.println("나이는 숫자로 입력하세요.");
        }

    }
}
