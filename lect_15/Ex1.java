// 사람: 이름, 주민 번호
// equals(): 주민 번호가 같으면 같은 사람으로 한다.
class Person implements Comparable{
    protected String name;
    protected int idNumber;
    protected int height; // 키가 큰 사람이 더 큰 사람으로 정의 ?

    public Person() {
        this.name = "";
        this.idNumber = -1;
        this.height = 0;
    }

    public Person(String name, int idNumber, int height) {
        this.name = name;
        this.idNumber = idNumber;
        this.height = height;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Person) {
            if(this.idNumber == ((Person)obj).idNumber) {
                return true;
            }
            else return false;
        }
        else {
            return false;
        }
    }

    public int compareTo(Object obj) {
        if(obj instanceof Person) {
            return this.height - ((Person)obj).height;
        }
        else {
            return 404;
        }
    }
}

class PersonTest {
    public static void main(String[] args) {
        Person p1, p2, p3;
        p1 = new Person();
        p2 = new Person();
        p3 = new Person("홍길동", 20170111, 170);

        System.out.println("p1, p2 동일 인물 ? : " + p1.equals(p2));
        System.out.println("p1, p3 동일 인물 ? : " + p1.equals(p3));

        // p1과 p3 중 누가 더 큰 사람인가?
        if(p1.compareTo(p3) < 0) {
            // p3가 더 크다
            System.out.println("p3가 더 크다.");
        }
        else if(p1.compareTo(p3) > 0) {
            // p1이 더 크다.
            System.out.println("p1이 더 크다.");
        }
        else {
            System.out.println("키가 같다.");
        }
    }
}


// 학생:
