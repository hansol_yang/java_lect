class Employee {
    protected String name;
    protected int age;
    protected int pay;

    public Employee() {}
    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("이름: %s, 나이: %d, ", this.name, this.age);
    }
}

class Regular extends Employee {
    public Regular() {}
    public Regular(String name, int age, int pay) {
        super(name, age);
        this.pay = pay;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("월급: %d\n", this.pay);
    }
}

class Temporary extends Employee {
    int time;

    public Temporary() {}
    public Temporary(String name, int age, int time) {
        super(name, age);
        this.time = time;
        this.pay = time * 10000;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("일한 시간: %d, 월급: %d\n", this.time, this.pay);
    }
}

public class Testt {
    public static void main(String[] args) {
        Regular r = new Regular("홍길동", 25, 2000000);
        System.out.print(r);
        Temporary t = new Temporary("장발장", 35, 100);
        System.out.print(t);
    }
}
