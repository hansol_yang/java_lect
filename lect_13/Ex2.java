class Pet {}
class Person {}
class Cat extends Pet {}
class Dog extends Pet {}
class Student extends Person {}

class Test2 {
    public static void main(String[] args) {
        Pet p2 = new Cat();

        // p2가 Pet형이다.
        // 만약 p2가 고양이라면 뭔가를 처리하고 싶다.

        if(p2 instanceof Cat) {
            System.out.printf("p2는 Cat의 객체입니다.\n");
        }
        else if(p2 instanceof Dog) { // true || false || 컴파일 오류
            System.out.printf("p2는 Dog의 객체입니다.\n");
        }
        else if(p2 instanceof Person) { // Pet 과 Person은 상속 관계가 없기 때문에 컴파일 오류

        }
    }
}
