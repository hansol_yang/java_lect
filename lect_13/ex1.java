class Pet {
    void speak() {}
}
class Person {

}
class Dog extends Pet {
    void sing() {}
}
class Cat extends Pet {

}
class Student extends Person {
    void sing() {}
}

class Test {
    public static void main(String[] args) {
        // 컴파일 시에는 캐스팅할 때, 상속 관계만 따짐
        // 실행할 때는 객체를 보고 판단(엄격)


        // Cat c = new Pet(); // disable
        Cat c = (Cat)new Pet(); // able -> 실행 시, 예외 발생
        // Dog d = new Pet(); //disable
        Dog d = (Dog)new Pet(); // disable
        d.speak();
        d.sing();

        // Dog d1 = new Person(); // disable -> 캐스팅 자체가 불가능(상속 관계가 아님)

        // Student d3 = new Dog(); // disable -> 캐스팅 자체가 불가능(상속 관계가 아님)

        Person p = new Student(); // able

    }
}
